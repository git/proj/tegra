# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

DESCRIPTION="NVIDIA Tegra2 X.org driver"

HOMEPAGE="http://developer.nvidia.com/content/linux-tegra-release-12-alpha-1-released"
SRC_URI="http://developer.download.nvidia.com/assets/mobile/files/tegra-linux-12.alpha.1.0.tar.gz"


LICENSE="nvidia"
SLOT="0"
KEYWORDS="arm"

IUSE=""
DEPEND="=x11-base/xorg-server-1.10*
	=sys-libs/tegra-libs-${PV}"
RDEPEND="${DEPEND}"

S="${WORKDIR}"
RESTRICT="strip mirror"

src_unpack() {
	unpack ${A}
	cd ldk/nv_tegra
	unpack ./tegra-drivers-abi10.tbz2
}

src_install() {
	cd ldk/nv_tegra
	insinto /
	doins -r usr
}
