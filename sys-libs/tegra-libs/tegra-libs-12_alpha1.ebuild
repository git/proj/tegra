# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

DESCRIPTION="NVIDIA Tegra2 libraries"

HOMEPAGE="http://developer.nvidia.com/content/linux-tegra-release-12-alpha-1-released"
SRC_URI="http://developer.download.nvidia.com/assets/mobile/files/tegra-linux-12.alpha.1.0.tar.gz"


LICENSE="nvidia"
SLOT="0"
KEYWORDS="arm"

IUSE=""
DEPEND=""
RDEPEND="${DEPEND}"

S="${WORKDIR}"
RESTRICT="strip mirror"

src_unpack() {
	unpack ${A}
	cd ldk/nv_tegra
	unpack ./tegra_bins.tar.gz
}

src_install() {
	cd ldk/nv_tegra
	# We have x11-drivers/tegra-drivers for this
	rm -rf usr/lib/xorg/

	# These collide with mesa :(
	rm usr/lib/libEGL.so

	insinto /
	doins -r usr lib
}
